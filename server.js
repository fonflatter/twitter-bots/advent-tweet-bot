const axios = require('axios')
const Twitter = require('twitter')

const now = new Date()

const year = now.getFullYear() % 100
const month = now.getMonth() + 1
const day = now.getDate()

const isDecember = month === 12
const isAdvent = isDecember && (day < 25)

if (!isAdvent) {
  throw new Error('Es ist gar nicht Advent!')
}

const imageUrl = `https://www.fonflatter.de/adv${year}/${day}.png`
const text = `Heute öffnet sich das ${day}. Türchen im fetzigen Fredventskalender!

Sämtliche 24 Comicstrips des Fredventskalenders könnt ihr übrigens bestellen. Als Heft. Sendet mir einfach eine Mail an fonflatter@gmail.com oder eine Direktnachricht.

http://www.fonflatter.de
`

const twitterSecrets = process.env.TWITTER_SECRETS
if (!twitterSecrets) {
  throw new Error('TWITTER_SECRETS ist nicht definiert!')
}

const twitterClient = new Twitter(JSON.parse(twitterSecrets))

uploadImage()
  .then(sendTweet)

function sendTweet (imageId) {
  return twitterClient.post('statuses/update', { status: text, media_ids: imageId })
    .then((tweet) => {
      const { text, user } = tweet
      const tweetUrl = `https://twitter.com/${user.name}/status/${tweet.id_str}`
      console.log({
        tweetUrl,
        text
      })
    })
}

function uploadImage () {
  return axios.get(imageUrl, {
    responseType: 'arraybuffer'
  })
    .then(({ data }) => {
      const imageData = Buffer.from(data, 'binary').toString('base64')
      return twitterClient.post('media/upload', { media_data: imageData })
        .then((media) => media.media_id_string)
    })
}